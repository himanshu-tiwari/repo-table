import React from 'react';
import { Table, Tag, Spin, Alert } from 'antd';
import './index.css';

const ReposTable = (props) => {
    const columns = [{
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
        render: text => <Tag><code>{text}</code></Tag>,
        sorter: (a, b) => a.name.length - b.name.length
    }, {
        title: 'Stars',
        dataIndex: 'stargazers_count',
        key: 'stars',
        defaultSortOrder: 'descend',
        sorter: (a, b) => a.stargazers_count - b.stargazers_count,
    }, {
        title: 'Forks',
        dataIndex: 'forks',
        key: 'forks',
        sorter: (a, b) => a.forks - b.forks,
    }, {
        title: 'Action',
        dataIndex: 'html_url',
        key: 'action',
        render: text => <a href={text} target="_blank" rel="noreferrer noopener">View</a>
    }];
    
    const { data, error, loading } = props;

    if (error) {
        return(
            <Alert
                message="Error"
                description={error}
                type="error"
                showIcon
            />
        );
    } else {
        if (loading) {
            return(
                <div className="table-container">
                    <p className="repo-count">{ (data && data.length) || 0 } public repositories</p>
                    <Spin>
                        <Table
                            columns={columns}
                            dataSource={data}
                            pagination={false}
                            bordered={true}
                            rowKey={record => record.id}
                            locale={{ emptyText: 'No data' }}
                        />
                    </Spin>
                </div>
            );
        } else {
            return(
                <div className="table-container">
                    <p className="repo-count">{ (data && data.length) || 0 } public repositories</p>
                    <Table
                        columns={columns}
                        dataSource={data}
                        pagination={false}
                        bordered={true}
                        rowKey={record => record.id}
                        locale={{ emptyText: 'No data' }}
                    />
                </div>
            );
        }
    }
};

export default ReposTable;