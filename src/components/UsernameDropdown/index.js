import React, { Component } from 'react';
import './index.css';
import { Menu, Dropdown, Button, Icon } from 'antd';

class UsernameDropdown extends Component {
    state = {
        username: null
    };

    handleMenuClick = (e) => {
        this.props.handleClick(e.item.props.value);

        this.setState({
            username: e.item.props.value
        });
    }
    
    render() {
        const { username } = this.state;

        const menu = (
            <Menu onClick={this.handleMenuClick}>
                <Menu.Item key="1" value="himanshu-tiwari"><Icon type="user" />himanshu-tiwari</Menu.Item>
                <Menu.Item key="2" value="aakashns"><Icon type="user" />aakashns</Menu.Item>
                <Menu.Item key="3" value="gaearon"><Icon type="user" />gaearon</Menu.Item>
                <Menu.Item key="4" value="ryanflorence"><Icon type="user" />ryanflorence</Menu.Item>
                <Menu.Item key="5" value="jph00"><Icon type="user" />jph00</Menu.Item>
                <Menu.Item key="6" value="sindresorhus"><Icon type="user" />sindresorhus</Menu.Item>
            </Menu>
        );

        return(
            <div className="dropdown-container">
                <Dropdown overlay={menu} trigger={['click']}>
                    <Button>
                        <div className="button-content">
                            <span>{ username || 'Select a User' }</span>
                            <div style={{flex: 1}}></div>
                            <Icon type="down" />
                        </div>
                    </Button>
                </Dropdown>
            </div>
        );
    }
};

export default UsernameDropdown;