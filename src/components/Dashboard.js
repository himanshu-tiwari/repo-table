import React from 'react';
import { Layout } from 'antd';
import './Dashboard.css';
import UsernameDropdown from './UsernameDropdown/index';
import ReposTable from './ReposTable';
import { connect } from 'react-redux';
import { getRepos } from '../store/actions/reposActions';

const { Content } = Layout;

const Dashboard = (props) => {
	const { data, error, loading } = props;
	const handleClick = (username) => {
		props.getRepos(username);
	};

    return(
        <Layout style={{ background: "#fff" }}>
            <Content className="dashboard-content">
                <h1 className="title">Github Repository Browser</h1>

                <UsernameDropdown handleClick={handleClick} />
                <ReposTable data={data} error={error} loading={loading} />
            </Content>
        </Layout>
    );
};

const mapStateToProps = (state) => {
	const { data, error, loading } = state.repos;
	return {
		data,
		error,
		loading
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		getRepos: username => dispatch(getRepos(username))
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);