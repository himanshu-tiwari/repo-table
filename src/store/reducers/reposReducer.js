const initState = {
	data: null,
	error: null,
	loading: false
}

const reposReducer = (state = initState, action) => {
	switch (action.type) {
		case 'REPO_FETCH_LOADING': 
			return {
				...state,
				loading: true
			};

		case 'REPO_FETCH_ERROR':
			return {
				...state,
				loading: false,
				error: action.err
			};
		
		case 'REPO_FETCH_SUCCESS':
			return {
				...state,
				loading: false,
				data: action.payload.data.data,
				error: null
			};

		default:
			return state
	}
};

export default reposReducer;