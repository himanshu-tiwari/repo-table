import axios from 'axios';

export const getRepos = (username) => {
	const url = username ? 'https://api.github.com/users/'+username+'/repos' : 'https://api.github.com/users/himanshu-tiwari/repos';
	
	return (dispatch, getState) => {
		dispatch({ type: 'REPO_FETCH_LOADING' });

		axios({
			url: url,
			method: 'get'
		}).then((res) => {
			if (res.statusText && res.statusText === "OK" && res.data) {
				dispatch({ type: 'REPO_FETCH_SUCCESS', payload: { data: res } });
			} else {
				dispatch({ type: 'REPO_FETCH_ERROR', err: res.data.msg });
			}
		}).catch((err) => {
			dispatch({ type: 'REPO_FETCH_ERROR', err });
		});
	}
}