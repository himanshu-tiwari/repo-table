import React from 'react';
import Dashboard from './components/Dashboard';

const App = (props) => {
	return (
		<div className="App">
			<Dashboard />
		</div>
	);
}

export default App;
